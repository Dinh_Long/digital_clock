import React, { Component } from 'react';
import './App.css';

class Eight extends Component {
  constructor(props) {
    super(props);
    this.state = {
      elem1: "",
      elem2: "",
      elem3: "",
      elem4: "",
      elem5: "",
      elem6: "",
      elem7: ""
    };
  }
  build() {
    switch (this.props.value) {
      case 0: {
        this.setState({
          elem1: "",
          elem2: "",
          elem3: "",
          elem4: "",
          elem5: "none",
          elem6: "",
          elem7: ""
        });
        break;
      }
      case 1: {
        this.setState({
          elem1: "none",
          elem2: "none",
          elem3: "",
          elem4: "none",
          elem5: "none",
          elem6: "",
          elem7: "none"
        });
        break;
      }
      case 2: {
        this.setState({
          elem1: "none",
          elem2: "",
          elem3: "",
          elem4: "",
          elem5: "",
          elem6: "none",
          elem7: ""
        });
        break;
      }
      case 3: {
        this.setState({
          elem1: "none",
          elem2: "",
          elem3: "",
          elem4: "none",
          elem5: "",
          elem6: "",
          elem7: ""
        });
        break;
      }
      case 4: {
        this.setState({
          elem1: "",
          elem2: "none",
          elem3: "",
          elem4: "none",
          elem5: "",
          elem6: "",
          elem7: "none"
        });
        break;
      }
      case 5: {
        this.setState({
          elem1: "",
          elem2: "",
          elem3: "none",
          elem4: "none",
          elem5: "",
          elem6: "",
          elem7: ""
        });
        break;
      }
      case 6: {
        this.setState({
          elem1: "",
          elem2: "",
          elem3: "none",
          elem4: "",
          elem5: "",
          elem6: "",
          elem7: ""
        });
        break;
      }
      case 7: {
        this.setState({
          elem1: "none",
          elem2: "",
          elem3: "",
          elem4: "none",
          elem5: "none",
          elem6: "",
          elem7: "none"
        });
        break;
      }
      case 8: {
        this.setState({
          elem1: "",
          elem2: "",
          elem3: "",
          elem4: "",
          elem5: "",
          elem6: "",
          elem7: ""
        });
        break;
      }
      case 9: {
        this.setState({
          elem1: "",
          elem2: "",
          elem3: "",
          elem4: "none",
          elem5: "",
          elem6: "",
          elem7: "none"
        });
        break;
      }
      default: {

      }
    }
  }
  componentDidMount() {
    setInterval(() => { this.build() }, 1000);
  }
  render() {
    return (
      <li>
        <div className="rectangle">
          <div>
            <div className="Rectangle-vertical left " style={{ display: this.state.elem1 }}></div>
            <div className="Rectangle-horizontal" style={{ display: this.state.elem2 }}></div>
            <div className="Rectangle-vertical right" style={{ display: this.state.elem3 }}></div>
          </div>
        </div>
        <div className="rectangle">
          <div>
            <div className="Rectangle-vertical left" style={{ display: this.state.elem4 }}></div>
            <div className="Rectangle-horizontal" style={{ display: this.state.elem5 }}></div>
            <div className="Rectangle-vertical right" style={{ display: this.state.elem6 }}></div>
          </div>
        </div>
        <div className="rectangle">
          <div className="Rectangle-horizontal" style={{ display: this.state.elem7 }}></div>
        </div>
      </li >
    );
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hour: 0,
      minute: 0,
      second: 0
    };
    {
      let date;
      setInterval(() => {
        date = new Date();
        this.setState({
          hour: date.getHours(),
          minute: date.getMinutes(),
          second: date.getSeconds()
        })
      }, 1000);
    }
  }

  render() {
    return (
      <ul>
        <Eight value={Math.floor(this.state.hour / 10)} />
        <Eight value={this.state.hour % 10} />
        <Eight value={Math.floor(this.state.minute / 10)} />
        <Eight value={this.state.minute % 10} />
        <Eight value={Math.floor(this.state.second / 10)} />
        <Eight value={this.state.second % 10} />
      </ul>
    );
  }
}

export default App;
